const array = [
  46, 71, 62, 78, 95, 97, 35, 77, 88, 99, 67, 73, 65, 8, 84, 66, 92, 61, 72, 22,
];
const result = "8,22,35,46,61,62,65,66,67,71,72,73,77,78,84,88,92,95,97,99";


// Bubble Sort
function bubbleSort(arr) {
  for (let n = arr.length - 1; n > 0; n--) {
    for (let i = 0; i < n; i++) {
      if (arr[i] > arr[i + 1]) {
        let temp = arr[i];
        arr[i] = arr[i + 1];
        arr[i + 1] = temp;
      }
    }
  }
  return arr;
}

test("Bubble sort", () => {
  const testArr = [...array];
  expect(bubbleSort(testArr).join()).toBe(result);
});

// Quicksort
function quickSort(arr) {
  if(arr.length < 2) {
    return arr;
  } else {
    const pivot = arr[Math.floor(Math.random() * arr.length)];
  	const less = arr.filter(value => value < pivot);
    const more = arr.filter(value => value > pivot);
    return [...quickSort(less), pivot, ...quickSort(more)];
  	}
};

test("Quicksort", () => {
  const testArr = [...array];
  expect(quickSort(testArr).join()).toBe(result);
});

// Selection sort
function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let indexMin = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[indexMin] > arr[j]) {
        indexMin = j;
      }
    }
    if (indexMin !== i) {
      let temp = arr[i];
      arr[i] = arr[indexMin];
      arr[indexMin] = temp;
    }
  }
  return arr;
};

test("Selection sort", () => {
  const testArr = [...array];
  expect(selectionSort(testArr).join()).toBe(result);
});

// Insertion sort
function insertionSort(arr) {
  for (let i = 1, l = arr.length; i < l; i++) {
    const current = arr[i];
    let j = i;
    while (j > 0 && arr[j - 1] > current) {
        arr[j] = arr[j - 1];
        j--;
    }
    arr[j] = current;
  }
  return arr;
};

test("Insertion sort", () => {
  const testArr = [...array];
  expect(insertionSort(testArr).join()).toBe(result);
});

// Merge sort
function merge(arrFirst, arrSecond) {
  const arrSort = [];
  let i = 0; 
  let j = 0;
  while (i < arrFirst.length && j < arrSecond.length) {
      if(arrFirst[i] < arrSecond[j]) {
        arrSort.push(arrFirst[i]);
        i++;
      } else {
        arrSort.push(arrSecond[j]);
        j++;
      }
  }
  return [
      ...arrSort,
      ...arrFirst.slice(i),
      ...arrSecond.slice(j)
  ];
};

function mergeSort(arr) {
  if (!arr || !arr.length) {
      return null;
  }
  if (arr.length <= 1) {
      return arr;
  }
  const middle = Math.floor(arr.length / 2);
  const arrLeft = arr.slice(0, middle);
  const arrRight = arr.slice(middle);
  return merge(mergeSort(arrLeft), mergeSort(arrRight));;
};

test("Merge sort", () => {
  const testArr = [...array];
  expect(mergeSort(testArr).join()).toBe(result);
});
